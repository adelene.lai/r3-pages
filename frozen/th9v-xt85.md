---
layout: default
order: -1
title: Molecular reclassification to find clinically useful biomarkers for systemic autoimmune diseases
permalink: /frozen/th9v-xt85
---


{% rtitle PRECISESADS: Molecular reclassification to find clinically useful biomarkers for systemic autoimmune diseases %}
[https://www.imi.europa.eu/projects-results/project-factsheets/precisesads](https://www.imi.europa.eu/projects-results/project-factsheets/precisesads)

PRECISESADS cohort includes whole blood molecular information for seven systemic autoimmune disorders (Systemic Lupus Erythematosus, Rheumatoid Arthritis, Sjogrën’s Syndrome, Systemic Sclerosis, Mixed Connective Tissue Disease, Primary Antiphospholipid Syndrome and Undifferentiated Connective Tissue Disease) and healthy controls. A cross-sectional study and an inception study prospectively followed for 6 and 14 months are included in the dataset. The molecular information comprise transcriptome, methylome, flow-cytometry and other serological information as cytokines and autoantibodies.

permalink: [doi:10.17881/th9v-xt85](https://doi.org/10.17881/th9v-xt85)
{% endrtitle %}


{% rblock Data access %}
Data is available upon request to PRECISESADS sustainability committee (please contact [via email](mailto:tania.gomes@genyo.es)). The data sustainability is provided by ELIXIR Luxembourg via its [data hosting services](https://elixir-luxembourg.org/services).

<br><br>
<div align="center">
<img src="{{ "images/elixir-logo.svg" | relative_url }}" width="20%" />
</div>

{% endrblock %}

{% rblock Publications | fas fa-scroll %}
Integrative Analysis Reveals a Molecular Stratification of Systemic Autoimmune Diseases. Guillermo Barturen, Sepideh Babaei, Francesc Català-Moll et al. medRxiv 2020.02.21.20021618; doi: [https://doi.org/10.1002/art.41610](https://doi.org/10.1002/art.41610)
{% endrblock %}

