---
layout: default
order: -1
title: Single-cell transcriptomics reveals multiple neuronal cell types in human midbrain-specific organoids
permalink: /frozen/single-cell-transcriptomics-reveals-multiple-neuronal-cell-types-in-human-midbrain-specific-organoids/
---


{% rtitle SINGLE-CELL TRANSCRIPTOMICS REVEALS MULTIPLE NEURONAL CELL TYPES IN HUMAN MIDBRAIN-SPECIFIC ORGANOIDS %}
Please cite the article on [BioRxiv](https://www.biorxiv.org/content/10.1101/589598v1).

Lisa M Smits†, Magni Stefano, Grzyb Kamil, Antony Paul MA, Krüger Rejko, Skupin Alexander, Bolognin Silvia, Jens C Schwamborn*
{% endrtitle %} 


{% rgridblock a-unique-id %}
{% rblock  SOURCE CODE | fas fa-code %}
The source code used to make the publication is available on [Github](https://github.com/LCSB-DVB/Smits_Reinhardt_2019) where you can traceback what have been done by the authors.
{% endrblock %}

{% rblock  SINGLE-CELL TRANSCRIPTOMICS | fas fa-th %}
Data is accessible through [LCSB WebDAV](https://webdav-r3lab.uni.lu/public/data/single-cell-transcriptomics-reveals-multiple-neuronal-cell-types-in-human-midbrain-specific-organoids/single-cell_transcriptomics/) website.
{% endrblock %}

{% rblock  RAW DATA | fas fa-image %}
The complete **Dataset** is available [here](https://webdav-r3lab.uni.lu/public/data/single-cell-transcriptomics-reveals-multiple-neuronal-cell-types-in-human-midbrain-specific-organoids/raw-data/).
{% endrblock %}
{% endrgridblock %} 
