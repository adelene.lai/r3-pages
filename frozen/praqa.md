---
layout: default
order: -1
title: "PRAQA: Protein Relative Abundance Quantification Algorithm for 3D Fluorescent Images from Tissue"
permalink: /frozen/PRAQA/
---



{% rtitle Protein Relative Abundance Quantification Algorithm for 3D  Fluorescent Images from Tissue %}
Corrado Ameli, Sonja Fixemer, David Bouvier, Alexander Skupin.

In confocal fluorescent microscopy, the quality of the acquisition strongly depends on diverse factors including
the microscope parameterization, the light exposure time, the type and concentration of the antibodies used,
the thickness of the sample and the degradation of the biological tissue itself. All these factors critically
influence the final result and render tissue protein quantification challenging due to intra- and inter-sample
variability. Therefore, image processing techniques need to address the acquisitions variability to minimize the
risk of bias coming from changes in signal intensity, noise and parameterization. Here, we introduce Protein
Relative Abundance Quantification Algorithm (PRAQA), a 1-parameter based, fast and adaptive approach for
quantifying protein abundance in 3D fluorescent-immunohistochemistry stained tissues that requires no image
preprocessing. Our method is based on the assessment of the global pixel intensity neighborhood dispersion
that allows to statistically infer whether each small region of an image can be considered as positive signal
or background noise. We benchmark our method with alternative approaches from literature and validate its
applicability and efficiency based on synthetic scenarios and a real-world application to post-mortem human
brain samples of Alzheimer’s Disease and Lewy Body Dementia patients.

permalink: [doi:10.17881/j20h-pa27](https://doi.org/10.17881/j20h-pa27)
{% endrtitle %} 


{% rgridblock a-unique-id %}


{% rblock source code %}

The source code for the program is available on [Github](https://github.com/CorradoAmeliLU/PRAQA).

{% endrblock %}

{% endrgridblock %} 
