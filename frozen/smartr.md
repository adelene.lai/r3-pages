---
layout: default
order: -1
title: Smartr
permalink: /frozen/smartr
---


{% rtitle SmartR: An open-source platform for interactive visual analytics for translational research data %}
Please cite the article on [Oxford Bioinformatics](https://doi.org/10.1093/bioinformatics/btx137).

Sascha Herzinger, Wei Gu, Venkata Satagopam, Serge Eifes, Kavita Rege, Adriano Barbosa-Silva, Reinhard Schneider, on behalf of the eTRIKS Consortium
{% endrtitle %} 

{% rgridblock a-unique-id %}
{%  rblock Website | fas fa-globe %}
The **SmartR** website hosted by **R3lab** is available [here](http://smartr.lcsb.uni.lu/).
{% endrblock %}


{%  rblock Source code | fas fa-code %}
The source code of SmartR is available on [GitHub](https://github.com/transmart/SmartR).
{% endrblock %}


{%  rblock Documentation | fas fa-book %}
All available documentation can be found [here](https://github.com/transmart/SmartR/tree/master/doc)
{% endrblock %}


{%  rblock Videos | fas fa-video %}
Several videos have been recorded to showcase the capabilities of SmartR. They can be accessed via [YouTube](https://www.youtube.com/playlist?list=PLNvp9GB9uBmH79puxxIHh9TOwQHTA_Ucy).
{% endrblock %}
{% endrgridblock %} 


 


 
