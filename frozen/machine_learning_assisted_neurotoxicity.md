---
layout: default
order: -1
title: Machine learning-assisted neurotoxicity prediction in human midbrain organoids  (PRE-VERSION)
permalink: /frozen/machine-learning-assisted-neurotoxicity-prediction-in-human-midbrain-organoids/
---


{% rtitle Machine learning-assisted neurotoxicity prediction in human midbrain organoids  (PRE-VERSION) %}
Please cite the article on [Paper](#).

Anna S. Monzel†, Kathrin Hemmer, Tony Kaoma Mukendi, Philippe Lucarelli, Isabel Rosety, Alise Zagare, Silvia Bolognin, Paul Antony, Sarah L. Nickels, Rejko Krueger, Francisco Azuaje, Jens C. Schwamborn*
{% endrtitle %} 


{% rgridblock a-unique-id %}
{% rblock  SOURCE CODE | fas fa-code %}
The source code is available on [Github](https://github.com/LCSB-DVB/Monzel_2019) where you can traceback what have been done by the authors. Similarly, you can find the code used for the machine learning analysis [here](https://gitlab.com/biomodlih/tox-ml).
{% endrblock %}

{% rblock  Figures | fas fa-image %}
The data for the figures can be found [here](https://webdav-r3lab.uni.lu/public/data/machine-learning-assisted-neurotoxicity-prediction-in-human-midbrain-organoid/).
{% endrblock %}

{% endrgridblock %} 
