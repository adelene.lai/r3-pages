---
layout: default
order: -1
title: Synapse alterations precede neuronal damage and storage pathology in a human cerebral organoid model of CLN3-juvenile neuronal ceroid lipofuscinosis
permalink: /frozen/synapse-alterations-precede-neuronal-damage-and-storage-pathology-in-a-human-cerebral-organoid-model/
---


{% rtitle Synapse alterations precede neuronal damage and storage pathology in a human cerebral organoid model of CLN3-juvenile neuronal ceroid lipofuscinosis %}
Please cite the article on [Paper](https://actaneurocomms.biomedcentral.com/articles/10.1186/s40478-019-0871-7).

Gemma Gomez-Giro†, Jonathan Arias-Fuenzalida, Javier Jarazo, Dagmar Zeuschner, Muhammad Ali, Nina Possemis, Silvia Bolognin, Rashi Halder, Christian Jäger, Willemijn F.E. Kuper, Holm Zaehres, Antonio del Sol, Herman van der Putten, Hans R. Schöler & Jens C. Schwamborn*
{% endrtitle %} 


{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-th %}
The complete dataset is available [here](https://webdav-r3lab.uni.lu/public/data/synapse-alterations-precede-neuronal-damage-and-storage-pathology-in-a-human-cerebral-organoid-model/). It is subdivided into originals (raw data) and partials (analysis) specific to each figure and supplementary present in the manuscript.
{% endrblock %}

{% rblock  RNA-Seq data | fas fa-th %}
The RNA-seq data is accessible through the [NCBI GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE137420) website.
{% endrblock %}

{% rblock  Source code | fas fa-copy %}
The source code used to make the analysis/figures of the publication is available on [GitHub](https://github.com/LCSB-DVB/Gomez-Giro_2019).
{% endrblock %}
{% endrgridblock %} 
