---
layout: default
order: -1
title: Clustering and visualizing huge-scale cytometry datasets with GigaSOM.jl
permalink: /frozen/gigasom
---

{% rtitle Clustering and visualizing huge-scale cytometry datasets with GigaSOM.jl %}

Miroslav Kratochvíl, Oliver Hunewald, Laurent Heirendt, Vasco Verissimo, Jiří Vondrášek, Venkata P. Satagopam, Reinhard Schneider, Christophe Trefois, Markus Ollert
<center>
<img src="{{ "images/logo-GigaSOM.jl.png" | relative_url }}" width="90%" />
</center>
{% endrtitle %}

{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-video %}
The used datasets are available on FlowRepository, under accession IDs [FR-FCM-ZZPH](http://flowrepository.org/id/FR-FCM-ZZPH)
and [FR-FCM-ZYX9](http://flowrepository.org/id/FR-FCM-ZYX9).
{% endrblock %}

{% rblock source code %}
**GigaSOM.jl** source code is hosted on [Github](https://github.com/LCSB-BioCore/GigaSOM.jl).
**GigaScatter.jl**, used for dataset rasterization, is also hosted on [Github](https://github.com/LCSB-BioCore/GigaScatter.jl).

{% endrblock %}

{% rblock documentation | fas fa-book %}
The documentation of **GigaSOM.jl** is available [here](https://git.io/GigaSOM.jl).

{% endrblock %}

{% endrgridblock %}