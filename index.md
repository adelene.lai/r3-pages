---
layout: default
title: Index
order: 1
---

# Reproducibility
Here you can find a collection of tools used @LCSB to provide support for reproducibility and transparency.

## Associated services

<style>
.block {
    width: 20%;
    max-width: 200px;
    min-width: 190px;
    border: solid 2px #f3f3f3;
    border-radius: 10px;
    padding: 15px;
    margin: 2px;
}
.block .image-box {
    height: 100px;
}
.block .image-box img {
    margin: 0px auto;
    display: block;
}
</style>

<div style="display: flex; flex-wrap: wrap">
  <div class="block">
     <div class="image-box">
        <a href="https://git-r3lab.uni.lu/">
          <img src="{{ "images/gitlab-logo.png" | relative_url }}" width="120px" />        
        </a>
     </div>        
     <a href="https://git-r3lab.uni.lu/"><strong>Gitlab</strong></a> is an open source software that provide Version control and many other usefule features for devellopers and scientists. 
  </div>
  <div class="block">
     <div class="image-box">                
       <a href="https://owncloud.lcsb.uni.lu/">
         <img src="{{ "images/owncloud-logo.png" | relative_url }}" width="150px" />        
       </a>
     </div>
     <a href="https://owncloud.lcsb.uni.lu/"><strong>OwnCloud</strong></a> is an online file sharing platform that makes collaboration over documents easier.
  </div>
  <div class="block">
     <div class="image-box">                
       <a href="https://asperaweb.lcsb.uni.lu/">
         <img src="{{ "images/aspera-logo.png" | relative_url }}" width="180px" />        
       </a>
     </div>
     <a href="https://asperaweb.lcsb.uni.lu/"><strong>AsperaWEB</strong></a> is hi-speed file transfer software, allowing for efficient exchange of very large files.
  </div>
  <div class="block">        
     <div class="image-box">        
       <a href="https://webdav-r3lab.uni.lu/">
         <img src="{{ "images/webdav-logo.png" | relative_url }}" width="150px" />        
       </a>
     </div>
     <a href="https://webdav-r3lab.uni.lu/"><strong>WEBDAV</strong></a> is a file sharing system over the web that brings more advanced security and control that a traditionnal FTP server.        
  </div>
</div>
